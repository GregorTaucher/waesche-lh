/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.test;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author GregorTaucher
 */
@ManagedBean
@SessionScoped
public class PricelistBean {
    private List<Product> productList = new ArrayList<>();
 
    public PricelistBean() {
        productList.add(new Product(1001, "electric toothbrush", 23.20));
        productList.add(new Product(1002, "teeth whitening strips", 10.90));
        productList.add(new Product(1007, "dental floss", 1.29));
        productList.add(new Product(1321, "electric shaver", 129.90));
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

   
 
    
}
